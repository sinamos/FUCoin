## Freie Universität Berlin cryptocurrency (fucoin)



### How to run?

We assume that the project is based in `<project-folder>` and maven is as command line tool installed.

```
$> cd <project-folder>
$> mvn compile
$> mvn exec:java
```


### Dependencies

* Java 8
* Maven
* Akka Actor