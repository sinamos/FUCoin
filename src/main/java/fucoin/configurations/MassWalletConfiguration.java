package fucoin.configurations;

import akka.dispatch.OnSuccess;
import fucoin.actions.transaction.ActionGetAmountAnswer;
import fucoin.configurations.internal.ConfigurationName;

/**
 * This configuration spawns 200 wallets to demonstrate the spawning of many headless wallets
 */
@ConfigurationName("Lots of Wallets")
public class MassWalletConfiguration extends AbstractConfiguration {
    @Override
    public void run() {
        initSupervisor();
        try {
            spawnWallets(2, false);
            System.out.println("Wallet spawning done!");
        } catch (Exception e) {
            System.out.println("Wallet spawning timed out!");
        }

        randomTransactions(5, 2).onSuccess(new OnSuccess<Void>() {
            @Override
            public void onSuccess(Void result) {
                // You can start your algorithm here if you want to.
                // Alternatively, you can also notify the user that all transactions are finished
                System.out.println("All random transactions finished!");
            }
        }, context().dispatcher());
    }

    @Override
    public void onReceive(Object message) {
        super.onReceive(message);
    }
}
