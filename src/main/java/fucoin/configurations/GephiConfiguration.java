package fucoin.configurations;

import akka.actor.ActorRef;
import akka.dispatch.OnSuccess;
import akka.pattern.Patterns;
import akka.util.Timeout;
import fucoin.actions.transaction.ActionGetAmount;
import fucoin.actions.transaction.ActionGetAmountAnswer;
import fucoin.configurations.internal.ConfigurationName;
import fucoin.configurations.internal.GephiLoader;
import fucoin.configurations.internal.NodeHelper;
import fucoin.gui.gephi.GephiFileSelector;
import fucoin.gui.gephi.GraphWindow;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

@ConfigurationName("Gephi Test Configuration")
public class GephiConfiguration extends AbstractConfiguration {

    private Timeout timeout = new Timeout(Duration.create(10, "seconds"));

    @Override
    public void run() {
        initSupervisor();

        GephiLoader gephiLoader = new GephiLoader();
        GephiFileSelector fileSelector = new GephiFileSelector();

        Graph g;
        File selectedTopology;
        try {
            selectedTopology = fileSelector.selectTopology();
            g = gephiLoader.loadFile(selectedTopology);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
            return;
        }

        createOverlayNetwork(g);

        GraphWindow graphWindow = new GraphWindow();
        graphWindow.setDisplayedFilename(selectedTopology.getName());
        graphWindow.setVisible(true);

        // add a click listener for displaying further information about a wallet when clicking on a node
        graphWindow.addNodeClickHandler((node, event) -> {

            // get associated wallet and ask for its amount
            ActorRef wallet = getWalletForNode(node);

            Future<Object> future = Patterns.ask(wallet, new ActionGetAmount(), timeout);
            future.onSuccess(new OnSuccess<Object>() {
                @Override
                public void onSuccess(Object result) throws Throwable {
                    // display the amount when an answer is received
                    ActionGetAmountAnswer answer = (ActionGetAmountAnswer) result;
                    graphWindow.setInfobarText(NodeHelper.nameOfNode(node)+" has "+answer.amount+" FUCs");
                }
            }, context().dispatcher());
        });

    }
}
