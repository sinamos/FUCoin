package fucoin.configurations;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.dispatch.Futures;
import akka.pattern.Patterns;
import akka.util.Timeout;
import fucoin.AbstractNode;
import fucoin.actions.control.ActionAddOverlayNeighbours;
import fucoin.actions.control.ActionAnnounceWalletCreation;
import fucoin.actions.control.ActionWalletSendMoney;
import fucoin.actions.join.ActionTellSupervisor;
import fucoin.actions.transaction.ActionGetAmount;
import fucoin.actions.transaction.ActionGetAmountAnswer;
import fucoin.actions.transaction.ActionNotifyObserver;
import fucoin.configurations.internal.ConfigurationCreator;
import fucoin.configurations.internal.NodeHelper;
import fucoin.supervisor.SuperVisorImpl;
import fucoin.wallet.WalletImpl;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.Promise;
import scala.concurrent.duration.Duration;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 *
 */
public abstract class AbstractConfiguration extends AbstractNode {

    private ActorRef superVisor;

    private final HashMap<String, ActorRef> activeActors = new HashMap<>();

    private Timeout timeout = new Timeout(Duration.create(10, "seconds"));

    private int remainingTransactions;
    private Promise<Void> transactionFinished;

    private ActorRef lastWallet = null;

    public static Props props(Class configurationClass) {

        return Props.create(new ConfigurationCreator(configurationClass));
    }

    /**
     * Spawns a new wallet and blocks until it has received its initial money
     *
     * @throws Exception on timeout
     */
    ActorRef spawnWallet(String name, boolean createGUI) throws Exception {
        Future<Object> future = Patterns.ask(superVisor, new ActionAnnounceWalletCreation(1, self()), timeout);
        ActorRef wallet = createWallet(name, createGUI);
        Await.result(future, timeout.duration());
        return wallet;
    }

    /**
     * Creates a wallet without blocking until the wallet was created
     */
    private ActorRef createWallet(String name, boolean createGUI) {
        Props props;

        props = WalletImpl.props(lastWallet, name, createGUI);

        ActorRef actorRef = context().actorOf(props, name);
        activeActors.put(name, actorRef);

        if (lastWallet == null) {
            actorRef.tell(new ActionTellSupervisor(superVisor), superVisor);
        }

        lastWallet = actorRef;

        return actorRef;
    }

    /**
     * Spawn multiple wallets and wait until they all have their initial FUC
     *
     * @throws Exception on timeout
     */
    public void spawnWallets(int n, boolean createGUI) throws Exception {
        Future<Object> future = Patterns.ask(superVisor, new ActionAnnounceWalletCreation(n, self()), timeout);
        for (int i = 0; i < n; i++) {
            String nameOfTheWallet = "Wallet" + String.valueOf(activeActors.size());
            createWallet(nameOfTheWallet, createGUI);
        }
        Await.result(future, timeout.duration());
    }

    public void spawnWalletsFromNodes(Collection<Node> nodes, boolean createGUI) throws Exception {
        Future<Object> future = Patterns.ask(superVisor, new ActionAnnounceWalletCreation(nodes.size(), self()), timeout);
        for (Node node : nodes) {
            String nameOfTheWallet = NodeHelper.nameOfNode(node);
            createWallet(nameOfTheWallet, createGUI);
        }
        Await.result(future, timeout.duration());
    }

    /**
     * Fetch a random wallet
     */
    public ActorRef getRandomWallet() {
        return wallets().get(ThreadLocalRandom.current().nextInt(activeActors.size()));
    }

    public List<ActorRef> wallets() {
        return new ArrayList<>(this.activeActors.values());
    }

    public ActorRef walletByName(String name) {
        return activeActors.get(name);
    }

    protected Future<Void> randomTransactions(int number, int maxTransactionsAtTheSameTime) {
        this.transactionFinished = Futures.promise();
        remainingTransactions = number;

        for (int i = 0; i < Math.min(number, maxTransactionsAtTheSameTime); i++) {
            nextRandomTransaction();
        }

        return transactionFinished.future();

    }

    private void nextRandomTransaction() {
        remainingTransactions--;
        try {
            randomTransaction();
        } catch (Exception e) {
            System.err.println("Error while trying to perform a random transaction: " + e.getMessage());
            remainingTransactions = 0;
            transactionFinished = null;
        }
    }

    private void randomTransaction() throws Exception {
        List<ActorRef> wallets = wallets();
        Collections.shuffle(wallets);

        ActorRef sender = wallets.get(0);
        ActorRef recipient = wallets.get(1);


        Future<Object> future = Patterns.ask(sender, new ActionGetAmount(), timeout);
        ActionGetAmountAnswer answer = (ActionGetAmountAnswer) Await.result(future, timeout.duration());

        int transferAmount = 1 + ThreadLocalRandom.current().nextInt(answer.amount);

        sender.tell(new ActionWalletSendMoney(recipient.path().name(), transferAmount, self()), self());
    }


    /**
     * Create the supervisor node
     */
    ActorRef initSupervisor() {
        superVisor = context().actorOf(SuperVisorImpl.props(), "SuperVisorImpl");

        // Don't ask.
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return superVisor;
    }

    @Override
    public void onReceive(Object message) {
        if (message instanceof ActionNotifyObserver) {

            ActionNotifyObserver notification = (ActionNotifyObserver) message;

            String status = "successful";
            if (!notification.granted) {
                status = "failed";
            }

            System.out.println("Observed a " + status + " transaction of " + notification.amount + " FUCs from " +
                    notification.source.path().name() + " to " + notification.target.path().name());

            if (remainingTransactions > 0) {
                nextRandomTransaction();
            } else {
                if (transactionFinished != null) {
                    transactionFinished.success(null);
                    transactionFinished = null;
                }
            }
        }
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();

        this.run();
    }

    public abstract void run();

    protected void createOverlayNetwork(Graph g) {
        Collection<Node> nodes = g.getNodes().toCollection();

        try {
            spawnWalletsFromNodes(nodes, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        nodes.stream().forEach(node -> {
            ActorRef wallet = getWalletForNode(node);
            Edge[] edges = g.getEdges(node).toArray();

            // Search for all reachable neighbours of the node
            // by filtering the list of incident edges of the node
            // and retrieve the respective ActorRef instance
            List<ActorRef> overlayNeighbours = Arrays.stream(edges).filter(edge -> !edge.isDirected() || edge.getSource() == node).map(edge -> {
                if (edge.getSource() == node) {
                    return edge.getTarget();
                }
                return edge.getSource();
            }).map(this::getWalletForNode).collect(Collectors.toList());

            wallet.tell(new ActionAddOverlayNeighbours(overlayNeighbours), self());
        });
    }

    protected ActorRef getWalletForNode(Node node) {
        return walletByName(NodeHelper.nameOfNode(node));
    }
}
