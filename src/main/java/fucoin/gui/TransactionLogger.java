package fucoin.gui;

/**
 * All nodes implementing some kind of logging should implement this interface.
 */
public interface TransactionLogger {

    /**
     * Adds a debug log message or a generic log message to the log
     * @param message
     */
    public void addLogMsg(String message);

    /**
     * Adds a log message concerning a successful transaction
     * @param message
     */
    public void addTransactionLogMessageSuccess(String message);

    /**
     * Adds a log message concerning a failed transaction
     * @param message
     */
    public void addTransactionLogMessageFail(String message);
}
