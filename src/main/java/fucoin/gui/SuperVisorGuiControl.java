package fucoin.gui;

public interface SuperVisorGuiControl extends TransactionLogger {

    /**
     * Call from SuperVisorImpl after poison pill or kill
     */
    void onLeave();

    public void updateTable(String address, String name, int amount);

}
