package fucoin.gui;

import javax.swing.*;
import java.awt.*;

public class LogCellRenderer extends DefaultListCellRenderer {

    private static final Color SUCCESS_COLOR = new Color(56, 127, 56);
    private static final Color FAIL_COLOR = new Color(217, 83, 79);

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        if (value instanceof LogMessage) {
            LogMessage msg = (LogMessage) value;

            switch (msg.getContext()) {

                case TRANSACTION_SUCCESS:
                    setForeground(SUCCESS_COLOR);
                    break;
                case TRANSACTION_FAIL:
                    setForeground(FAIL_COLOR);
                    break;
                case DEBUG:
                    break;
            }
        }

        if (isSelected) {
            setForeground(Color.WHITE);
        }

        return this;
    }
}
