package fucoin.gui;


public class LogMessage {

    public enum Context {
        TRANSACTION_SUCCESS, TRANSACTION_FAIL, DEBUG
    }

    private String message;
    private Context context;

    public LogMessage(String message) {
        this(message, Context.DEBUG);
    }

    public LogMessage(String message, Context context) {
        this.message = message;
        this.context = context;
    }

    public String getMessage() {
        return message;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
