package fucoin.gui.gephi;

import org.gephi.graph.api.Node;
import org.gephi.preview.api.PreviewMouseEvent;

import java.util.function.BiConsumer;


public interface NodeClickHandler extends BiConsumer<Node, PreviewMouseEvent> {
}
