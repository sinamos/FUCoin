package fucoin.gui.gephi;

import org.gephi.graph.api.Node;
import org.gephi.preview.api.*;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;
import java.util.Timer;

public class GraphWindow extends JFrame implements NodeMouseListener {

    protected List<NodeClickHandler> clickHandlers = new ArrayList<>();
    private final PreviewController previewController;
    private final PreviewSketch previewSketch;

    private JLabel infobarText;
    private Timer timer;

    private final String baseWindowTitle = "Network Overlay Graph";
    private final String defaultInfoBarText = "Click on a node to see further information.";

    public GraphWindow() {
        super();

        timer = new Timer();

        setTitle(baseWindowTitle);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        previewController = Lookup.getDefault().lookup(PreviewController.class);
        PreviewModel previewModel = previewController.getModel();
        previewModel.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(Color.BLACK));

        Font labelFont = new Font("Verdana", Font.PLAIN, 5);
        previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, labelFont);
        previewModel.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
        previewModel.getProperties().putValue(PreviewProperty.EDGE_OPACITY, 50);
        previewModel.getProperties().putValue(PreviewProperty.EDGE_RADIUS, 10f);
        previewModel.getProperties().putValue("graphWindow.mouse.handler", this);

        G2DTarget target = (G2DTarget) previewController.getRenderTarget(RenderTarget.G2D_TARGET);
        previewSketch = new PreviewSketch(target, isRetina());

        infobarText = new JLabel(defaultInfoBarText, SwingConstants.LEFT);

        this.add(previewSketch, BorderLayout.CENTER);

        JPanel infobar = new JPanel(new BorderLayout());
        infobar.add(infobarText, BorderLayout.WEST);

        JPanel zoomOptions = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 2));
        zoomOptions.add(new JLabel("Zoom: "));

        Dimension zoomBtnSize = new Dimension(20, 20);

        JButton minusButton = new JButton("-");
        minusButton.setPreferredSize(zoomBtnSize);
        minusButton.addActionListener(e -> previewSketch.zoomMinus());
        zoomOptions.add(minusButton);

        JButton resetButton = new JButton("0");
        resetButton.setPreferredSize(zoomBtnSize);
        resetButton.addActionListener(e -> previewSketch.resetZoom());
        zoomOptions.add(resetButton);

        JButton plusButton = new JButton("+");
        plusButton.setPreferredSize(zoomBtnSize);
        plusButton.addActionListener(e -> previewSketch.zoomPlus());
        zoomOptions.add(plusButton);

        infobar.add(zoomOptions, BorderLayout.EAST);
        this.add(infobar, BorderLayout.SOUTH);

        previewController.refreshPreview();
        previewSketch.resetZoom();


        this.setSize(800, 600);
    }

    public void setDisplayedFilename(String filename) {
        setTitle(baseWindowTitle + " - " + filename);
    }

    public static boolean isRetina() {
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        final GraphicsDevice device = env.getDefaultScreenDevice();

        try {
            Field field = device.getClass().getDeclaredField("scale");

            if (field != null) {
                field.setAccessible(true);
                Object scale = field.get(device);

                if (scale instanceof Integer && (Integer) scale == 2) {
                    return true;
                }
            }
        } catch (Exception ignore) {
        }
        return false;
    }

    @Override
    public void mouseClicked(Node node, PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {
        clickHandlers.stream().forEach(nodeClickHandler -> nodeClickHandler.accept(node, event));
    }

    public void addNodeClickHandler(NodeClickHandler handler) {
        clickHandlers.add(handler);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);

        previewController.refreshPreview();
        previewSketch.refreshSketch();
    }

    /**
     * Sets the displayed text of the infobar to text.
     * After a certain time the text will be reset to the default text.
     *
     * @param text new infobar text
     */
    public void setInfobarText(String text) {
        SwingUtilities.invokeLater(() -> infobarText.setText(text));
        // set text back to default text after 2 seconds
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                SwingUtilities.invokeLater(() -> infobarText.setText(defaultInfoBarText));
            }
        }, 2000);
    }
}
