package fucoin.gui;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class FilteredLogModel extends AbstractListModel<LogMessage> {

    private List<LogMessage> log;
    private List<LogMessage> filteredLog;

    private boolean filterTransactions = false;

    public FilteredLogModel() {
        super();
        this.log = new ArrayList<>();
        filteredLog = new ArrayList<>();
    }

    public void setTransactionFilter() {
        filterTransactions = true;
        refilter();
    }

    public void clearFilter(){
        filterTransactions = false;
        refilter();
    }

    private void refilter() {
        filteredLog.clear();
        for (LogMessage msg : log) {
            if (matchesFilter(msg)) {
                filteredLog.add(msg);

            }
        }
        fireContentsChanged(this, 0, getSize()-1);
    }

    private boolean matchesFilter(LogMessage msg){
        return !filterTransactions
                || (msg.getContext() == LogMessage.Context.TRANSACTION_SUCCESS
                || msg.getContext() == LogMessage.Context.TRANSACTION_FAIL);
    }

    @Override
    public int getSize() {
        return filteredLog.size();
    }

    @Override
    public LogMessage getElementAt(int index) {
        return filteredLog.get(index);
    }


    public void addElement(LogMessage message) {

        log.add(message);

        if(matchesFilter(message)){
            filteredLog.add(message);
            int index = getSize() - 1;
            fireIntervalAdded(this, index, index);
        }
    }

    public void emptyLog() {
        log.removeAll(log);
        filteredLog.removeAll(filteredLog);
        refilter();
    }
}
