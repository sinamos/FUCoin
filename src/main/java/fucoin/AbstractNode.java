package fucoin;

import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.UntypedActor;
import fucoin.actions.transaction.ActionGetAmount;
import fucoin.wallet.AbstractWallet;

import java.io.Serializable;
import java.util.HashMap;

public abstract class AbstractNode extends UntypedActor implements Serializable {

    /**
     * Returns the akka-style address as String,
     * which could be converted to an ActorRef object later
     *
     * @return
     */
    public String getAddress() {
        return getAddress(getSelf());
    }

    public String getAddress(ActorRef self) {
        Address remoteAddr = getContext().system().provider().getDefaultAddress();
        return self.path().toStringWithAddress(remoteAddr);
    }

    /**
     * The which receives Action objects
     *
     * @param message
     */
    public abstract void onReceive(Object message);

    /**
     * Holds references to neighbors that were in contact with this wallet during runtime;
     * The key corresponds to the WalletImpl's name
     */
    private transient HashMap<String, ActorRef> knownNeighbors = new HashMap<String, ActorRef>();

    /**
     * Holds references to neighbors
     * this wallet synchronizes itself to (the WalletImpl object);
     * The key corresponds to the WalletImpl's name
     */
    public transient HashMap<String, ActorRef> localNeighbors = new HashMap<String, ActorRef>();

    /**
     * Holds all Wallets from network participants
     * which synchronize their state (WalletImpl object) with us;
     * The key corresponds to the WalletImpl's name
     */
    public transient HashMap<String, AbstractWallet> backedUpNeighbors =
            new HashMap<String, AbstractWallet>();

    public transient HashMap<ActorRef, Integer> amounts = new HashMap<ActorRef, Integer>();

    public boolean addKnownNeighbor(String key, ActorRef value) {
        if (!knownNeighbors.containsKey(key)) {
            knownNeighbors.put(key, value);
            value.tell(new ActionGetAmount(), getSelf());
            return true;
        }
        return false;
    }

    public HashMap<String, ActorRef> getKnownNeighbors() {
        return knownNeighbors;
    }
}