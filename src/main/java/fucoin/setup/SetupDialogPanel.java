package fucoin.setup;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.util.List;

public class SetupDialogPanel extends JPanel {
    private JTextField walletNameField = new JTextField(10);
    private JTextField pathField = new JTextField(10);
    private JComboBox<SelectableNetworkInterface> hostnameField = new JComboBox<>();

    public SetupDialogPanel(String defaultHostname) {
        super();

        createComponents(defaultHostname);
    }

    private void createComponents(String defaultHostname) {
        this.setLayout(new GridLayout(3, 1));
        this.add(new JLabel("Pick your wallet name: ", SwingConstants.LEFT));
        this.add(walletNameField);
        this.add(new JLabel("Enter a neighbour node address: ", SwingConstants.LEFT));
        this.add(pathField);
        this.add(new JLabel("Select your reachable IP address: ", SwingConstants.LEFT));

        selectWalletNameField();

        List<SelectableNetworkInterface> interfaces = NetworkInterfaceReader.fetchNetworkInterfaces();

        for (SelectableNetworkInterface netint : interfaces) {
            hostnameField.addItem(netint);
        }

        hostnameField.setSelectedItem(new SelectableNetworkInterface(defaultHostname, "default"));
        this.add(hostnameField);
    }

    /**
     * This method makes sure, that the Wallet name field is selected on display of the dialog
     */
    private void selectWalletNameField() {
        walletNameField.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(AncestorEvent event) {
                final AncestorListener al = this;
                SwingUtilities.invokeLater(() -> {
                    JComponent component = event.getComponent();
                    component.requestFocusInWindow();
                    component.removeAncestorListener(al);
                });
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {

            }

            @Override
            public void ancestorMoved(AncestorEvent event) {

            }
        });
    }

    public String getWalletName() {
        return walletNameField.getText();
    }

    public String getAddressOfNeighbour() {
        return pathField.getText();
    }

    public SelectableNetworkInterface getNetworkInterface() {
        return (SelectableNetworkInterface) hostnameField.getSelectedItem();
    }
}
