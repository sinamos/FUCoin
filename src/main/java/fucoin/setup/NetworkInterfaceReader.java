package fucoin.setup;

import java.net.*;
import java.util.*;

public class NetworkInterfaceReader {
    /**
     * Grab a list of all network interfaces and the associated IP addresses to prevent some strange behaviours of
     * the <code>InetAddress.getLocalHost().getHostAddress();</code> call
     *
     * @return The list contains SelectableNetworkInterfaces, that contain the interface name and the associated IP address
     */
    public static List<SelectableNetworkInterface> fetchNetworkInterfaces() {
        List<SelectableNetworkInterface> map = new ArrayList<>();

        try {
            Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();

            for (NetworkInterface networkInterface : Collections.list(nets)) {
                Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                // To ease the setup, currently only IPv4 addresses are supported
                Optional<InetAddress> picked = Collections.list(inetAddresses).stream().filter(inetAddress -> !(inetAddress instanceof Inet6Address)).findFirst();
                if (picked.isPresent()) {
                    String hostAddress = picked.get().getHostAddress();
                    map.add(new SelectableNetworkInterface(hostAddress, networkInterface.getDisplayName()));
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        return map;
    }

    /**
     * Get the IP address, that is resolved by the system from the host name.
     * As a fallback, we return 127.0.0.1, i.e. localhost
     */
    public static String readDefaultHostname() {
        String hostname = "127.0.0.1";
        try {
            hostname = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return hostname;
    }
}
