package fucoin.actions.control;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.ClientAction;
import fucoin.wallet.AbstractWallet;

import java.util.Collections;
import java.util.List;

/**
 * @author davidbohn
 */
public class ActionAddOverlayNeighbours extends ClientAction {

    protected List<ActorRef> neighbours;

    public ActionAddOverlayNeighbours(List<ActorRef> neighbours) {
        this.neighbours = neighbours;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, AbstractWallet abstractNode) {
        neighbours.stream().forEach(abstractNode::addOverlayNeighbour);
    }
}
