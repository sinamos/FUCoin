package fucoin.actions.transaction;


import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.search.ActionSearchWalletReference;
import fucoin.wallet.AbstractWallet;

public class ActionInvokeSentMoney extends Transaction {
    public final String name;
    public final int amount;
    public final ActorRef observer;

    public ActionInvokeSentMoney(String name, int amount, ActorRef observer) {
        this.name = name;
        this.amount = amount;
        this.observer = observer;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.addLogMsg(wallet.getKnownNeighbors() + "");
        if (wallet.getKnownNeighbors().containsKey(name)) {
            wallet.getRemoteSuperVisorActor().tell(
                    new ActionInvokeDistributedCommittedTransfer(self, wallet.getKnownNeighbors().get(name), amount, observer), sender);
        } else {
            ActionSearchWalletReference aswr = new ActionSearchWalletReference(name);
            for (ActorRef neighbor : wallet.getKnownNeighbors().values()) {
                neighbor.tell(aswr, self);
            }
            sleep(self, context, 200);
            self.tell(this, self);
        }
    }

    private void sleep(ActorRef self, UntypedActorContext context, int timeOfIdle) {
        try {
            context.unwatch(self);
            Thread.sleep(timeOfIdle);
            context.watch(self);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
