package fucoin.actions.transaction;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

public class ActionPrepareDistributedCommittedTransfer extends Transaction {

    private ActorRef source;
    private ActorRef target;
    private int amount;
    private long timestamp;
    private long id;

    public ActionPrepareDistributedCommittedTransfer(ActorRef source, ActorRef target,
                                                     int amount, long timestamp, long id) {
        this.source = source;
        this.target = target;
        this.amount = amount;
        this.timestamp = timestamp;
        this.id = id;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        boolean granted = amount > 0 &&
                //sender is supervisor(bank) has always money
                (sender.compareTo(source) == 0
                        //sender is unknown, might be valid
                        || (wallet.amounts.containsKey(source)
                        //sender have enough money
                        && wallet.amounts.getOrDefault(source, 0) >= amount));

        // precautionly update own ledger to prevent double spending (respectively agreeing)
        Integer sourceAmount = wallet.amounts.getOrDefault(source, 0);
        Integer targetAmount = wallet.amounts.getOrDefault(target, 0);
        wallet.amounts.put(source, sourceAmount - amount);
        wallet.amounts.put(target, targetAmount + amount);

        sender.tell(new ActionPrepareDistributedCommittedTransferAnswer(source, target, amount, timestamp, granted, id), self);
    }

}
