package fucoin.actions.transaction;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.AbstractNode;
import fucoin.actions.Action;

/**
 * Used to notify an observer of a transaction when the transaction is finished.
 */
public class ActionNotifyObserver extends Action<AbstractNode> {

    public ActorRef source;
    public ActorRef target;
    public int amount;
    public boolean granted;
    public long timestamp;
    public long id;

    public ActionNotifyObserver(ActorRef source, ActorRef target,
                                int amount, boolean granted, long timestamp, long id){
        this.source = source;
        this.target = target;
        this.amount = amount;
        this.granted = granted;
        this.timestamp = timestamp;
        this.id = id;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, AbstractNode abstractNode) {
    }
}
