package fucoin.actions.transaction;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;
import fucoin.wallet.WalletImpl;

public class ActionGetAmountAnswer extends Transaction {

    public String address;
    public String name;
    public int amount;

    public ActionGetAmountAnswer(String address, String name, int amount) {
        this.address = address;
        this.name = name;
        this.amount = amount;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.amounts.put(sender, amount);
    }

}
