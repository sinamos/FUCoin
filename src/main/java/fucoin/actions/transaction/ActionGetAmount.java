package fucoin.actions.transaction;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

public class ActionGetAmount extends Transaction {

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        ActionGetAmountAnswer agaa = new ActionGetAmountAnswer(wallet.getAddress(), wallet.getName(), wallet.getAmount());
        sender.tell(agaa, self);
    }

}
