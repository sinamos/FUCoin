package fucoin.actions.persist;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

public class ActionInvokeLeave extends Persist {

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        for (ActorRef neighbor : wallet.getKnownNeighbors().values()) {
            if (self.compareTo(neighbor) != 0) {
                neighbor.tell(new ActionStoreOrUpdate(wallet), self);
            }
        }


        wallet.setActive(false);
        wallet.backedUpNeighbors.clear();
        wallet.getKnownNeighbors().clear();
    }

}
