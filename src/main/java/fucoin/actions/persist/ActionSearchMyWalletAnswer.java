package fucoin.actions.persist;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

/**
 * Used to return a searched WalletImpl
 */
public class ActionSearchMyWalletAnswer extends Persist {
    public final AbstractWallet w;

    public ActionSearchMyWalletAnswer(AbstractWallet w) {
        this.w = w;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.setAmount(w.getAmount());
        sender.tell(new ActionInvalidate(wallet.getName()), self);
    }
}
