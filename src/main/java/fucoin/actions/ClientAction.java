package fucoin.actions;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

public abstract class ClientAction extends Action<AbstractWallet> {
    @Override
    protected abstract void onAction(ActorRef sender, ActorRef self,
                                     UntypedActorContext context, AbstractWallet abstractNode);

}
