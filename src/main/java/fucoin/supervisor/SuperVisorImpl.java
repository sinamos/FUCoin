package fucoin.supervisor;

import akka.actor.ActorRef;
import akka.actor.Props;
import fucoin.actions.Action;
import fucoin.actions.control.ActionWalletCreationDone;
import fucoin.actions.persist.ActionInvokeUpdate;
import fucoin.actions.transaction.ActionGetAmountAnswer;
import fucoin.actions.transaction.ActionNotifyObserver;
import fucoin.actions.transaction.SuperVisorAction;
import fucoin.gui.SuperVisorGuiControl;
import fucoin.AbstractNode;
import fucoin.gui.TransactionLogger;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SuperVisorImpl extends AbstractNode implements TransactionLogger {

    //private AmountTableModel amountTableModel;

    private Map<Long, DistributedCommittedTransferRequest> requestQueue;

    private SuperVisorGuiControl gui;

    private int pendingBankCommits = 0;
    private ActorRef bankCommitObserver = null;

    public SuperVisorImpl() {

    }

    public void setGuiControl(SuperVisorGuiControl gui) {
        this.gui = gui;
    }

    @Override
    public void onReceive(Object msg) {

        // dirty but necessary since ActionGetAmountAnswer is a
        // ClientAction for some reason
        if (msg instanceof ActionGetAmountAnswer) {
            ActionGetAmountAnswer answer = (ActionGetAmountAnswer) msg;
            SwingUtilities.invokeLater(() -> gui.updateTable(answer.address, answer.name, answer.amount));
        } else if (msg instanceof ActionNotifyObserver) {
            ActionNotifyObserver notifyMsg = (ActionNotifyObserver) msg;
            if (notifyMsg.granted) {
                if (pendingBankCommits > 0) {
                    pendingBankCommits--;
                } else {
                    return;
                }

                if (pendingBankCommits == 0) {
                    this.getBankCommitObserver().tell(new ActionWalletCreationDone(), self());
                }
            }
        } /* TODO: Whats happened here?? Why we can invoke doAction of abstract class? */ else if (msg instanceof SuperVisorAction) {
            ((Action) msg).doAction(this);
        }
    }

    public static Props props() {
        return Props.create(new SuperVisorCreator());
    }

    public void updateValues() {
        getSelf().tell(new ActionInvokeUpdate(), getSelf());
    }

    public void exit() {
        getContext().stop(getSelf());
        if (gui != null) {
            gui.onLeave();
        }
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
    }

    public void addDistributedCommitedTransferRequest(
            DistributedCommittedTransferRequest request) {
        requestQueue.put(request.getId(), request);
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();
        requestQueue = new HashMap<>();
        self().tell(new ActionUpdateQueue(), self());
    }

    /**
     * filters the request for outdated and removes them
     *
     * @return deleted outdated request
     */
    public List<DistributedCommittedTransferRequest> updateList() {
        List<Long> deletesIds = new ArrayList<>();
        List<DistributedCommittedTransferRequest> deletes = new ArrayList<>();
        for (Entry<Long, DistributedCommittedTransferRequest> outdatedRequest : requestQueue.entrySet()) {
            if (outdatedRequest.getValue().getTimeout() < System.currentTimeMillis()) {
                deletesIds.add(outdatedRequest.getKey());
                deletes.add(outdatedRequest.getValue());
            }
        }
        for (Long delete : deletesIds) {
            requestQueue.remove(delete);
        }

        return deletes;
    }

    public DistributedCommittedTransferRequest getRequest(Long id) {
        return requestQueue.get(id);
    }

    public void deleteRequest(DistributedCommittedTransferRequest request) {
        requestQueue.remove(request.getId());
    }

    @Override
    public void addLogMsg(String message) {
        if (gui != null) {
            gui.addLogMsg(message);
        } else {
            System.out.println(message);
        }
    }

    @Override
    public void addTransactionLogMessageSuccess(String message) {
        if (gui != null) {
            gui.addTransactionLogMessageSuccess(message);
        } else {
            System.out.println(message);
        }
    }

    @Override
    public void addTransactionLogMessageFail(String message) {
        if (gui != null) {
            gui.addTransactionLogMessageFail(message);
        } else {
            System.out.println(message);
        }
    }

    public int getPendingBankCommits() {
        return pendingBankCommits;
    }

    public ActorRef getBankCommitObserver() {
        return bankCommitObserver;
    }

    public void setPendingBankCommits(int pendingBankCommits) {
        this.pendingBankCommits = pendingBankCommits;
    }

    public void setBankCommitObserver(ActorRef bankCommitObserver) {
        this.bankCommitObserver = bankCommitObserver;
    }
}
